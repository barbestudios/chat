var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var allMessages = [];

io.on('connection', (client) => {
  console.log('Client connected...'); //La console voit s'il y a une connexion

  client.on('join', (name) => {//Lorsque le client join il envoit un emit qui est reçu ici
    client.nickname = name;

    client.emit('messages', allMessages); //envois tous les messages

    client.broadcast.emit('messages', {
      user: 'Bot', message:client.nickname+' vien de joindre la conversation.'
    }); //envois le message à tout le monde (sauf la personne qui l'envois)
    client.emit('messages', {
      user: 'Bot', message:client.nickname+' vous venez de joindre la conversation'
    }); //envois le message à tout le monde (sauf la personne qui l'envois)

    allMessages.push({user: 'Bot', message:client.nickname+' vous venez de joindre la conversation'});
  });

  client.on('disconnect', (name) => { //Lorsque le client se déconnecte un emit(par défaut) est envoyé et reçu ici
    client.broadcast.emit('messages', {
      user: 'Bot', message:client.nickname+' vient de partir. :('
    }); //envois le message à tout le monde (sauf la personne qui l'envois)

    allMessages.push({user: 'Bot', message:client.nickname+' vient de partir. :('});
    console.log('Client deconnected...');
  });

  client.on('message', (data) => { //Lorsque le client envois un message un emit est reçu ici
    console.log(data);
    var user = client.nickname;

    client.broadcast.emit('messages', {
      user: user, message:data
    }); //envois le message à tout le monde (sauf la personne qui l'envois)

    client.emit('messages', {
      user: user, message:data
    }); //nous renvois le message

    allMessages.push({user: user, message:data});
  });

});

//Route serveur
app.use(express.static(__dirname + '/')); //Permet d'ajouter la feuille de style

app.get('/', (req, res) => { // la route /
  res.sendFile(__dirname + '/index.html'); //Fait afficher la page index
});

server.listen(8080); //port 8080
console.log('Le serveur est lancé sur le port 8080 \n');
